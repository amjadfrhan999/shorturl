import React from "react";
import {useHistory} from "react-router";

const Info = () => {
    const history = useHistory();
    const handleStart = () => {
        history.push({
            pathname: '/Test',
            search: 'start'
        });
    }

    return (
        <section className={"container2"}>
            <br/><br/><br/>
            <section className={"info-w"}>
                <div id={"cont1"}>
                    <img style={{float:"right", width:"20%", padding:"0px 40px 0px 0px"}} src={require("./images/linkimg.png")} />
                    Short Url, big result</div>
                <div id={"cont2"}>A URL shortener built with powerful tools to help you grow and protect your brand.</div>
                <div className={"center"}>
                    <button className={"button"} onClick={handleStart}>Get Started for Free</button>
                </div>
            </section>
            <br/><br/><br/><br/>
        </section>
    );
}

export default Info
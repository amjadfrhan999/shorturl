import React, {useState} from "react";
import {TextField, Button, LinearProgress} from "@material-ui/core";
import shorturlc from "../API/shorturlc";



const HTTP_URL_VALIDATOR_REGEX = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;


const SearchUrl = () => {
    const [url, setUrl] = useState('');
    const [shortCode, setShortCode] = useState('');
    const [getUrl, setGetUrl] = useState(false);

    const validURL = (string) => {
        return string.match(HTTP_URL_VALIDATOR_REGEX)
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if(validURL(url)) {
            getLink();
            setUrl('')
            setGetUrl(!getUrl)
        }else {
            setShortCode('please enter a valid Url')
        }
    }

    const getLink = async () =>{
        await shorturlc
            .get(`shorten?url=${url}`)
            .then((response) => {
                setShortCode(response.data.result.short_link)
                setGetUrl(false)
                })
            .catch((error) => {
                console.error(error)
                })
    }

    return (
        <section>
            <form className={"SearchForm"} onSubmit={(e) => handleSubmit(e)} style={{display: 'flex', flexDirection: 'column'}}>
                <TextField
                    className={"textF"}
                    style={{marginBottom: '20px'}}
                    label={"Enter Your Link"}
                    variant={"outlined"}
                    value={url}
                    onChange={(e) => setUrl(e.target.value)}
                />

                {!getUrl && (
                    <Button
                        onClick={(e) => handleSubmit(e)}
                        style={{marginBottom: '20px', background: "cadetblue"}}
                        variant={"contained"}
                        >
                        Submit
                    </Button>
                )}


                {getUrl && <LinearProgress style={{barColorPrimary: "red"}}/>}

            </form>
            {shortCode && (
                <div>
                    Your short Url: {shortCode}
                </div>
            )}
        </section>
    )
}

export default SearchUrl
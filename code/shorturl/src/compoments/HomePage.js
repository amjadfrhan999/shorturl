import React, { Component } from "react";
import './HomePage.css'
import Footer from './Footer'
import Header from "./Header";
import SearchUrl from "./SearchUrl";
import Info from "./Info";
import MoreInfo from "./MoreInfo";


class HomePage extends React.Component{
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render(){
        return (
            <section>
                <section>
                    <Header  />
                </section>
                <section className={"container"}>
                    <section>
                        <Info  />
                    </section>
                    <section>
                        <SearchUrl  />
                    </section>
                    <section className={"container3"}>
                        <MoreInfo  />
                    </section>
                </section>
                <section>
                    <Footer  />
                </section>
            </section>
        );
    }
}
export default HomePage
import React from "react";
import {useHistory} from "react-router";

const MoreInfo = () => {
    const history = useHistory();
    const handleStart = () => {
        history.push({
            pathname: '/Test',
            search: 'start'
        });
    }

    return (
        <section>
            <br/>
            More than a free link shorter
            <button className={"success"} onClick={handleStart}>
                Get Started
            </button>
            <br/><br/><br/><br/>
        </section>
    );
}

export default MoreInfo
import './Footer.css'


const Footer = () => {
    return (
        <section className={"footer"}>
            <hr className={"footer-seperator"} />
            <section className={"footer-social"}>
                <a href={"https://www.linkedin.com/in/amjad-frhan-sfe/"}>social</a>
            </section>
            <section className={"footer-info"}>
                <section className={"footer-left"}>
                    <section className={"footer-name"}>
                        software enginner amjad
                    </section>
                    <section className={"footer-policy"}>
                        return policy
                        <br/>
                        delivary
                    </section>
                </section>
                <section className={"footer-center"}>
                    <section className={"footer-email"}>
                        amjadfrhan999@gmail.com
                    </section>
                    <section className={"footer-terms"}>
                        Terms
                        <br/>
                        copyright
                    </section>
                </section>
                <section className={"footer-right"}>
                    <section className={"footer-number"}>
                        050-459-1192
                    </section>
                    <section className={"footer-contact"}>
                        My Story
                        <br/>
                        contact us
                    </section>
                </section>
            </section>
            <hr className={"footer-seperator"} />

        </section>
    );
}

export default Footer
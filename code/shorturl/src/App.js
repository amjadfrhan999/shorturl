import './App.css';
import HomePage from "./compoments/HomePage";
import React from "react";
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import Test from "./compoments/Test";


function App() {
  return (
      <Router>
          <Switch>
              <Route path='/HomePage' component={HomePage}/>
              <Route path='/Test' component={Test}/>
              <Route exact path="/"> <Redirect to="/HomePage"/></Route>
          </Switch>
      </Router>
  );
}

export default App;
